# spark-monit-utilities

My suggestion for the version numbering is to go from "x.y" to "x.y+1", increasing the first number only for non-backward-compatible changes; in this way we know that we can always update a job project to use the last version of the library. Of course this could be disregarded if the numbers end up going too high.

When developing a new version, to run the pipeline you need to tag the development branch:

```
git tag <new-version>
git push origin <new-version>
```

To run the pipeline again for the same version, you need to update the tag:

```
git push origin :refs/tags/<new-version>
git tag <new-version> -f
git push origin <new-version>
```

Remember to also tag the master branch after the merge!
