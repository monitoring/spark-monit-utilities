package ch.cern.monitoring.utilities.logging

import org.apache.log4j.{Level, Logger, Priority}

case class LoggerWrapper(logger: Logger, className: String) {

  def log(message: AnyRef, priority: Priority = Level.DEBUG): Unit = {
    logger.log(priority, f"[$className] " + message)
  }

}
