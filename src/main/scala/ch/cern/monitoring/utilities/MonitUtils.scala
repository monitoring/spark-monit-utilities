package ch.cern.monitoring.utilities

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col

import java.time.Instant
import java.time.temporal.ChronoUnit

object MonitUtils {
  // The millisecond version is used when the timestamp column has the time in milliseconds
  def filterTimeIntervalMilliseconds(df: DataFrame, batch: Boolean, timestampColumn: String, watermarkHours: Option[Int],
                                     startOpt: Option[String], endOpt: Option[String]): DataFrame = {
    (startOpt, endOpt) match {
      case (None, None) => _filterTimeIntervalWatermarkMilliseconds(df, timestampColumn, watermarkHours.get, batch)
      case (Some(start), Some(end)) => _filterTimeIntervalStartEndMilliseconds(df, timestampColumn, start, end)
    }
  }

  // The seconds version is used when the timestamp column has the time in seconds
  def filterTimeIntervalSeconds(df: DataFrame, batch: Boolean, timestampColumn: String, watermarkHours: Option[Int],
                                startOpt: Option[String], endOpt: Option[String]): DataFrame = {
    (startOpt, endOpt) match {
      case (None, None) => _filterTimeIntervalWatermarkSeconds(df, timestampColumn, watermarkHours.get, batch)
      case (Some(start), Some(end)) => _filterTimeIntervalStartEndSeconds(df, timestampColumn, start, end)
    }
  }


  /**
    * Filter a dataframe on a time interval using a watermark
    */
  private def _filterTimeIntervalWatermarkMilliseconds(df: DataFrame, timestampColumn: String, watermarkHours: Int, batch: Boolean): DataFrame = {
    if (batch) {
      val startTime = Instant.now().minus(watermarkHours, ChronoUnit.HOURS).truncatedTo(ChronoUnit.HOURS)
      val endTime = Instant.now().truncatedTo(ChronoUnit.HOURS)
      df.filter(col(timestampColumn) >= startTime.toEpochMilli && col(timestampColumn) < endTime.toEpochMilli)
    } else {
      df
    }
  }

  /**
    * Filter a dataframe on a time interval using start and end times
    */
  private def _filterTimeIntervalStartEndMilliseconds(df: DataFrame, timestampColumn: String, start: String, end: String): DataFrame = {
    val startTime = Instant.parse(start).truncatedTo(ChronoUnit.HOURS)
    val endTime = Instant.parse(end).truncatedTo(ChronoUnit.HOURS)
    df.filter(col(timestampColumn) >= startTime.toEpochMilli && col(timestampColumn) < endTime.toEpochMilli)
  }

  /**
    * Filter a dataframe on a time interval using a watermark
    */
  private def _filterTimeIntervalWatermarkSeconds(df: DataFrame, timestampColumn: String, watermarkHours: Int, batch: Boolean): DataFrame = {
    if (batch) {
      val startTime = Instant.now().minus(watermarkHours, ChronoUnit.HOURS).truncatedTo(ChronoUnit.HOURS)
      val endTime = Instant.now().truncatedTo(ChronoUnit.HOURS)
      df.filter(col(timestampColumn) >= (startTime.toEpochMilli / 1000) && col(timestampColumn) < (endTime.toEpochMilli / 1000))
    } else {
      df
    }
  }

  /**
    * Filter a dataframe on a time interval using start and end times
    */
  private def _filterTimeIntervalStartEndSeconds(df: DataFrame, timestampColumn: String, start: String, end: String): DataFrame = {
    val startTime = Instant.parse(start).truncatedTo(ChronoUnit.HOURS)
    val endTime = Instant.parse(end).truncatedTo(ChronoUnit.HOURS)
    df.filter(col(timestampColumn) >= (startTime.toEpochMilli / 1000) && col(timestampColumn) < (endTime.toEpochMilli / 1000))
  }

}
