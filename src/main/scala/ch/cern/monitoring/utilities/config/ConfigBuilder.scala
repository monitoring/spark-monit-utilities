package ch.cern.monitoring.utilities.config

import scala.collection.mutable.Map
import scala.reflect._


/**
* Empty trait used to "tag" a Config class; make sure the Config class you are
* writing extends this trait.
*/
trait BaseConfig {}

/**
* Build a configuration using the scopt parser.
* To use this object, you need to create a separate case class for the job
* configuration (from now on, referred to as "Config.scala"). This Config class
* must extend the BaseConfig trait and have the following structure:
*   case class Config(
*     batch: Param[Boolean] = ("batch", true) // parameter without abbreviation
*     input: Param[String] = ('i',"input","Input parameter")
*     someNumber: Param[Int] = ('n',"number","A number parameter")
*   ) extends BaseConfig
* The main job application will do something like the following:
*   val config = ConfigBuilder.parseConfig(args, APPLICATION_NAME, Config()) match {
*     Some(c) => c.asInstanceOf[Config]
*     None => {
*       System.exit(1) // bad arguments
*       Config()
*     }
*   }
*/
object ConfigBuilder {
  // Base Scala types, supported by the scopt parser
  val _typesMap: Map[String, Any] = Map(
    "String" -> classTag[String].toString,
    "Int" -> classTag[Int].toString,
    "Long" -> classTag[Long].toString,
    "Double" -> classTag[Double].toString,
    "Boolean" -> classTag[Boolean].toString
  )

  /**
  * Parse the configuration from the provided arguments.
  * Note that the result is both in the passed "conf" parameter and returned
  * by the function wrapped in an option.
  *
  * @param args arguments to parse
  * @param applicationName
  * @param conf configuration to be filled with the parsed parameter
  * @return an Option wrapping the "conf" param after it has been filled
  */
  def parseConfig(args: Array[String], applicationName: String, conf: BaseConfig): Option[BaseConfig] = {
    // Extract fields into a map
    val _confFields = conf.getClass.getDeclaredFields
    val _fieldsMap: Map[String,Any] = Map()
    // Fill the map by casting the fields to their type based on Param::getType
    _confFields.foreach {
      case field => {
        field.setAccessible(true)
        // Can cast to Param[String] anyway as we only need the the .getType
        val fieldType = field.get(conf).asInstanceOf[Param[String]].getType
        fieldType match {
          case t if t == _typesMap("String") => _fieldsMap += (field.getName -> field.get(conf).asInstanceOf[Param[String]])
          case t if t == _typesMap("Int") => _fieldsMap += (field.getName -> field.get(conf).asInstanceOf[Param[Int]])
          case t if t == _typesMap("Long") => _fieldsMap += (field.getName -> field.get(conf).asInstanceOf[Param[Long]])
          case t if t == _typesMap("Double") => _fieldsMap += (field.getName -> field.get(conf).asInstanceOf[Param[Double]])
          case t if t == _typesMap("Boolean") => _fieldsMap += (field.getName -> field.get(conf).asInstanceOf[Param[Boolean]])
          case _ => ()
        }
      }
    }

    // Build parser reading the args into the map
    val parser = new scopt.OptionParser[Unit](applicationName) {
      head(applicationName, getClass.getPackage.getImplementationVersion)
      _confFields.foreach {
        case field => {
          field.setAccessible(true)
          val fieldType = field.get(conf).asInstanceOf[Param[String]].getType
          // Pick the right opt based on the Param type
          fieldType match {
            case t if t == _typesMap("String") => {
              val param = _fieldsMap(field.getName).asInstanceOf[Param[String]]
              if (param.allowAbbr) {
                opt[String](param.abbr, param.fullName)
                  .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                  .text(param.text)
              } else {
                opt[String](param.fullName)
                  .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                  .text(param.text)
              }
            }
            case t if t == _typesMap("Int") => {
              val param = _fieldsMap(field.getName).asInstanceOf[Param[Int]]
              if (param.allowAbbr) {
                opt[Int](param.abbr, param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              } else {
                opt[Int](param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              }
            }
            case t if t == _typesMap("Long") => {
              val param = _fieldsMap(field.getName).asInstanceOf[Param[Long]]
              if (param.allowAbbr) {
                opt[Long](param.abbr, param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              } else {
                opt[Long](param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              }
            }
            case t if t == _typesMap("Double") => {
              val param = _fieldsMap(field.getName).asInstanceOf[Param[Double]]
              if (param.allowAbbr) {
                opt[Double](param.abbr, param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              } else {
                opt[Double](param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              }
            }
            case t if t == _typesMap("Boolean") => {
              val param = _fieldsMap(field.getName).asInstanceOf[Param[Boolean]]
              if (param.allowAbbr) {
                opt[Boolean](param.abbr, param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              } else {
                opt[Boolean](param.fullName)
                .foreach(v => _fieldsMap(field.getName) = param.withValue(v))
                .text(param.text)
              }
            }
            case _ => ()
          }
        }
      }
    }

    // Parse the args and check for error
    parser.parse(args, ()) match {
      case Some(c) => {}  // config parsed successfully
      case None => {
        return None // arguments are bad
      }
    }

    // Fill the actual configuration with the values from the map
    _confFields.foreach {
      case field => field.set(conf, _fieldsMap(field.getName))
    }

    return Option[BaseConfig](conf)
  }

}
