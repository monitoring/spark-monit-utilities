package ch.cern.monitoring.utilities.config

import scala.reflect._
/**
* A parameter in a spark job-specific configuration.
* @param abbr char to specify the parameter (e.g. "-f" in command line)
* @param fullName extended parameter name (e.g. "--file" in command line)
* @param text human-readable description of the parameter
* A Param[T] also holds the following variables:
* @param optValue holds the value of the parameter in a Option[T]
* @param allowAbbr true if the abbreviation can be used in the command line
* @param paramType String representation of the type T, used for parsing
*
* The two recommended ways of defining a Param are the constructors in the
* object Param definition.
* The first allows for the standard parameter definition, specifying both The
* abbreviation and the full name; the second uses no abbreviation, forcing the
* user to specify the full parameter.
*
* The value of a Param can be obtained either as an Option[T] (when it's
* needed to check if it was specified) or directly as value, respectively with
* 'parameter.optValue' and 'parameter.value'.
*
*/

object Param {
  def apply[T: ClassTag](abbr: Char, fullName: String, text: String): Param[T] = {
    Param[T](abbr, fullName, text, None, allowAbbr = true, classTag[T].toString)
  }
  def apply[T: ClassTag](fullName: String, text: String): Param[T] = {
    Param[T]('?', fullName, text, None, allowAbbr = false, classTag[T].toString)
  }
}

// The Param value can be either checked directly with parameter.value or
// through the option with parameter.optValue
case class Param[T](abbr: Char, fullName: String, text: String, optValue: Option[T], allowAbbr: Boolean, paramType: String) {
  def value(): T = {
    optValue.get
  }
  def withValue(newValue: T): Param[T] = {
    Param[T](abbr, fullName, text, Option[T](newValue), allowAbbr, paramType)
  }
  def getType: String = {
    paramType
  }
}
