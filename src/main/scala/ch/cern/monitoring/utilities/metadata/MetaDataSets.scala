package ch.cern.monitoring.utilities.metadata

import ch.cern.monitoring.utilities.io.KafkaUtils
import ch.cern.monitoring.utilities.logging.LoggerWrapper
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

protected class MetaDataSets(kafka: KafkaUtils) {

  val log = LoggerWrapper(kafka.logger, this.getClass.toString)
  import kafka.spark.implicits._

  /**
    * Cric Topology DataSet containing only the most recent record per Name
    */
  def getCricTopology(partitionCols: Seq[String], vo: Option[String]): Dataset[Row] = {
    val CricTopologyRDD = {
      kafka
        .read("cric_raw_topology", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val CricTopologyRDDAggWindow =
      Window.partitionBy(partitionCols.map(x => col(x)): _*)
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dfTopology = kafka.spark.read.json(CricTopologyRDD)
                     .select(last($"data").over(CricTopologyRDDAggWindow).as("data"))
                     .dropDuplicates
                     .select($"data.*")

    if (vo.isDefined) {
        dfTopology.where("vo = '" + vo.get + "'")
    }

    val dataset = dfTopology
    log.log(f"CRIC topology retrieved for vo: $vo with partition columns: $partitionCols")
    dataset
  }

  /**
    * Cric Topology Timeseries DataSet containing the full series available in Kafka
    */
  def getCricTopologySeries(vo: Option[String]): Dataset[Row] = {
    val CricTopologyRDD = {
      kafka
        .read("cric_raw_topology", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val dfTopology = kafka.spark.read.json(CricTopologyRDD)
                      .dropDuplicates
                      .select($"data.*",$"metadata.timestamp")

    if (vo.isDefined) {
      dfTopology.where("vo = '" + vo.get + "'")
    }
    val dataset = dfTopology
    log.log(f"CRIC topology time series retrieved for vo: $vo")
    dataset
  }


  /**
    * Rebus Federations DataFrame containing only the most recent record per Site
    */
  lazy val rebusFederations: DataFrame = {
    val rebusFederationsRDD = {
      kafka
        .read("rebus_raw_federations", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val rebusAggWindow =
      Window.partitionBy($"data.site")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataframe =
      kafka.spark
      .read
      .json(rebusFederationsRDD)
      .select(last($"data").over(rebusAggWindow).as("data"))
      .dropDuplicates()
      .select($"data.*")

    log.log(f"Rebus Federation records retrieved from Kafka")
    dataframe
  }

  /**
    * Agis Topology DataFrame containing only the most recent record per ExperimentSite
    */
  lazy val agisTopology: DataFrame = {
    val agisTopologyRDD = {
      kafka
        .read("agis_raw_topology", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val agisTopologyAggWindow = Window.partitionBy($"data.experiment_site")
      .orderBy($"metadata.timestamp")
      .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataframe =
      kafka.spark
      .read
      .json(agisTopologyRDD)
      .select(last($"data").over(agisTopologyAggWindow).as("data"))
      .dropDuplicates()
      .select($"data.*")

    log.log(f"Agis topology retrieved from Kafka")
    dataframe
  }

  /**
    * Agis Pandaqueue DataFrame containing only the most recent record per Pattern
    */
  lazy val agisPandaqueue: DataFrame = {
    val agisPandaqueueDs =
      kafka
        .read("agis_raw_pandaqueue", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .map(row => row.getAs[String]("value"))

    val agisPandaqueueAggWindow =
      Window.partitionBy($"data.pattern")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataframe = kafka.spark
      .read
      .json(agisPandaqueueDs)
      .select(last($"data").over(agisPandaqueueAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .withColumnRenamed("pattern", "computingsite")

    log.log(f"Agis Panda Queue retrieved from Kafka")
    dataframe
  }

  /**
    * Agis DDM Endpoint DataSet containing only the most recent record per Name
    */
  lazy val agisDDMEndpoint: Dataset[Row] = {
    val agisDDMEndpointRDD = {
      kafka
        .read("agis_raw_ddmendpoint", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val agisDDMEndpointAggWindow =
      Window.partitionBy($"data.name")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataset = kafka.spark
      .read
      .json(agisDDMEndpointRDD)
      .select(last($"data").over(agisDDMEndpointAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .dropDuplicates()

    log.log(f"Agis DDM endpoints retrieved from Kafka")
    dataset
  }

  /**
    * CRIC topology extra DataSet containing only the most recent record per Name
    */
  lazy val cricTopologyExtra: Dataset[Row] = {
    val cricTopologyExtraRDD = {
      kafka
        .read("cric_raw_extra", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val cricTopologyExtraAggWindow =
      Window.partitionBy($"data.experiment_site")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataset =
      kafka.spark
      .read
      .json(cricTopologyExtraRDD)
      .select(last($"data").over(cricTopologyExtraAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .dropDuplicates()

    log.log(f"CRIC extra topology retrieved from Kafka")
    dataset
  }

  /**
    * CRIC DDM Endpoint DataSet containing only the most recent record per Name
    */
  lazy val cricDDMEndpoint: Dataset[Row] = {
    val cricDDMEndpointRDD = {
      kafka
        .read("cric_raw_ddmendpoint", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val cricDDMEndpointAggWindow =
      Window.partitionBy($"data.name")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataset =
      kafka.spark
      .read
      .json(cricDDMEndpointRDD)
      .select(last($"data").over(cricDDMEndpointAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .dropDuplicates()

    log.log(f"CRIC DDM endpoints retrieved from Kafka")
    dataset
  }

  /**
    * CRIC Pandaqueue DataSet containing only the most recent record per Name
    */
  lazy val cricPandaQueue: Dataset[Row] = {
    val cricPandaQueueRDD = {
      kafka
        .read("cric_raw_pandaqueue", Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val cricPandaQueueAggWindow =
      Window.partitionBy($"data.pattern")
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val dataframe =
      kafka.spark
      .read
      .json(cricPandaQueueRDD)
      .select(last($"data").over(cricPandaQueueAggWindow).as("data"))
      .dropDuplicates
      .select($"data.*")
      .withColumnRenamed("pattern", "computingsite")
      .dropDuplicates()

    log.log(f"CRIC Panda Queue retrieved from Kafka")
    dataframe
  }

  /**
    * Full Topology DataSet containing unique results from the union of Rebus and Agis Datasets
    * In case of duplicated records, Rebus' one has higher preference
    */
  lazy val fullTopology: DataFrame = {
    val rebus = rebusFederations.select(
      $"country",
      $"real_federation".as("federation"),
      $"site".as("official_site")
    )

    val agis = agisTopology.select(
      $"country",
      $"federation",
      $"official_site"
    )

    // Fuse together REBUS and AGIS metadata
    // See spark 2.1 docs: .union(..) is equivalent to UNION ALL, we need to drop duplicates here
    val dataframe = rebus.union(agis)
      .groupBy("official_site")
      .agg(
        first($"country").as("country"),
        first($"federation").as("federation"),
        $"official_site")

    log.log(f"Rebus+Agis topology retrieved from Kafka")
    dataframe
  }

  /**
    * Topology DataSet that provides flatten result out of the VO or CRIC data records.
    * It return only the most recent records per OfficialSite
    */
  def getFlattenTopology(topic: String, partitionCols: Seq[String], vo: Option[String]): Dataset[Row] = {
    val flattenTopologyRDD = {
      kafka
        .read(topic, Map("startingOffsets" -> "earliest"))
        .select($"value".cast("string"))
        .rdd
        .map(row => row.getAs[String]("value"))
    }

    val metadataAggWindow =
      Window.partitionBy(partitionCols.map(x => col(x)): _*)
        .orderBy($"metadata.timestamp")
        .rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)

    val flattenTopology = kafka.spark
      .read
      .json(flattenTopologyRDD)
      .select(last($"data").over(metadataAggWindow).as("data"))
      .dropDuplicates()
      .withColumn("service", explode($"data.services"))
      .select(
        $"service.hostname",
        $"service.flavour".as("service_flavour"),
        $"data.experiment_site",
        $"data.official_site",
        $"data.tier",
        $"data.vo")
    var flattenTopologyFiltered = flattenTopology
    if (vo.isDefined) {
      flattenTopologyFiltered = flattenTopology.where("vo = '" + vo.get + "'")
    }
    val dataset = flattenTopologyFiltered
    log.log(f"Flat topology retrieved from Kafka")
    dataset
  }
}
