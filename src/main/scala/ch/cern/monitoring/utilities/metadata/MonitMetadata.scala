package ch.cern.monitoring.utilities.metadata

import ch.cern.monitoring.utilities.io.KafkaUtils
import ch.cern.monitoring.utilities.logging.LoggerWrapper
import org.apache.log4j.{Level, Logger, Priority}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, Row, SparkSession}

/** Monitoring Metadata collector class for structured streaming
  *
  * @param spark             SparkSession to be passed from the main job
  * @param kafkaInputBrokers string containing the Kafka broker URLs
  */
class MonitMetadata(kafka: KafkaUtils) {

  def this(spark: SparkSession, kafkaInputBrokers: String, logger: Logger, kafkaTopicPrefix: String = "") {

    this(new KafkaUtils(spark, kafkaInputBrokers, outputBrokers = "", logger, kafkaTopicPrefix))
  }

  import kafka.spark.implicits._

  val log = LoggerWrapper(kafka.logger, this.getClass.toString)
  val metaDataSets = new MetaDataSets(kafka)
  val sam3StatusMapping = Map(
    "CRITICAL" -> 0.0,
    "WARNING" -> 0.5,
    "OK" -> 1.0,
    "UNKNOWN" -> 2.0,
    "NODATA" -> 3.0,
    "DOWNTIME" -> 4.0,
    "DEFAULT" -> 5.0
  )

  /**
    * Gets persisted Cric metadata from Kafka, grouped by experiment site,
    * hostname and vo.
    *
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @return Cric dataset with columns: vo, [prefix]+(hostname, service_flavour, experiment_site,
    *         official_site, tier)
    */
  def getCricMetadata(prefix: String = "", vo: Option[String] = None): Dataset[Row] = {
    val prefixedColumns: List[String] = List(
      "hostname", "experiment_site", "service_flavour", "official_site",
      "tier","endpoint","country","federation","country_code"
    )
    val nonPrefixedColumns: List[String] = List(
      "status","institute_name","description","vo","state","in_report"
    )

    val partitionCols: List[String] = List(
      "data.endpoint","data.vo","data.experiment_site", "data.official_site", "data.flavour"
    )

    val df = metaDataSets.getCricTopology(partitionCols, vo)
      .withColumnRenamed("flavour", "service_flavour")
      .select(nonPrefixedColumns.map(s => col(s)) ::: prefixedColumns.map(s => col(s).as(prefix + s)): _*)

    if (df.head(1).isEmpty) {
      throw new Exception("getCricMetadata returned an empty dataframe")
    }

    val dataset = df.persist()
    log.log(f"CRIC metadata retrieved for vo: $vo with prefix: $prefix")
    dataset
  }

  /**
    * Gets persisted Cric metadata timeseries from Kafka
    *
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @return Cric dataset with columns: vo, timestamp, ... [prefix]+(hostname, service_flavour, experiment_site,
    *         official_site, tier)
    */
  def getCricTopologySeries(prefix: String = "", vo: Option[String] = None): Dataset[Row] = {
    val prefixedColumns: List[String] = List(
      "hostname", "experiment_site", "service_flavour", "official_site",
      "tier", "endpoint", "country", "federation", "country_code"
    )
    val nonPrefixedColumns: List[String] = List(
      "status", "institute_name", "description", "vo", "state", "in_report", "timestamp"
    )

    val df = metaDataSets.getCricTopologySeries(vo)
      .withColumnRenamed("flavour", "service_flavour")
      .select(nonPrefixedColumns.map(s => col(s)) ::: prefixedColumns.map(s => col(s).as(prefix + s)): _*)

    if (df.head(1).isEmpty) {
      throw new Exception("getCricTopologySeries returned an empty dataframe")
    }

    val dataset = df.persist()
    log.log(f"CRIC metadata time series retrieved for vo: $vo with prefix: $prefix")
    dataset
  }

  /**
    * Gets persisted CRIC ddmendpoints metadata from Kafka.
    * @return CRIC ddmendpoint dataset as read from Kafka
    */
  def getCricDDMEndpoints(): Dataset[Row] = {
    val dataset = metaDataSets.cricDDMEndpoint.persist()
    log.log(f"CRIC DDM endpoints metadata retrieved from Kafka")
    dataset
  }

  /**
    * Gets persisted CRIC pandaqueues metadata from Kafka.
    * @return CRIC pandaqueue dataset as read from Kafka
    */
  def getCricPandaQueues(): Dataset[Row] = {
    val dataset = metaDataSets.cricPandaQueue.persist()
    log.log(f"CRIC Panda Queues metadata retrieved from kafka")
    dataset
  }

  /**
    * Gets persisted CRIC extra topology metadata from Kafka.
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @param vo mandatory parameter to get the extra information for a specific vo
    * @return CRIC topology extra dataset as read from Kafka
    */
  def getCricTopologyExtra(prefix: String = "", vo: String): Dataset[Row] = {
    val prefixedColumns: List[String] = List("experiment_site", "cloud", "corepower")
    val nonPrefixedColumns: List[String] = List("vo")

    val df = metaDataSets.cricTopologyExtra
      .where("vo = '" + vo + "'")
      .select(nonPrefixedColumns.map(s => col(s)) ::: prefixedColumns.map(s => col(s).as(prefix + s)): _*)
    if (df.head(1).isEmpty) {
      throw new Exception("getCricTopologyExtra returned an empty dataframe")
    }

    val dataset = df.persist()
    log.log(f"CRIC extra topology retrieved for vo: $vo with prefix: $prefix")
    dataset
  }

  /**
    * Generic method to get persisted VoFeed metadata from Kafka, grouped by all fields in the result set
    * (experiment site,hostname, service_flavour, official_site, tier and vo).
    *
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @param vo     optional parameter to get only the sites from this vo
    * @return VoFeed dataset with columns: vo, [prefix]+(hostname, service_flavour, experiment_site,
    *         official_site, tier)
    */
  def getVoFeedMetadata(prefix: String = "", vo: Option[String] = None): Dataset[Row] = {
    val prefixedColumns: List[String] = List(
      "hostname", "experiment_site", "service_flavour", "official_site", "tier"
    )

    val partitionCols: List[String] = List(
      "data.experiment_site","data.services.hostname","data.services.flavour","data.official_site","data.tier","data.vo"
    )

    val df = metaDataSets.getFlattenTopology("vofeed_raw_topology", partitionCols, vo)
      .select(col("vo") :: prefixedColumns.map(s => col(s).as(prefix + s)): _*)
    if (df.head(1).isEmpty) {
      throw new Exception("getVoFeedMetadata returned an empty dataframe")
    }
    val dataset = df.persist()
    log.log(f"VoFeed metadata retrieved for vo: $vo with prefix: $prefix")
    dataset
  }

  /**
    * Gets persisted VoFeed metadata from Kafka, grouped by experiment site, hostname and vo.
    *
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @param vo     optional parameter to get only the sites from this vo
    * @return VoFeed dataset with columns: vo, [prefix]+(hostname, experiment_site, official_site, tier)
    */
  def getVoFeedByExperimentSiteAndHostname(prefix: String = "", vo: Option[String] = None): Dataset[Row] = {
    val prefixedColumns: List[String] = List(
      "hostname", "experiment_site", "official_site", "tier"
    )

    val partitionCols: List[String] = List(
      "data.experiment_site", "data.services.hostname", "data.vo"
    )

    val df = metaDataSets.getFlattenTopology("vofeed_raw_topology", partitionCols, vo)
      .select(col("vo") :: prefixedColumns.map(s => col(s).as(prefix + s)): _*)
    if (df.head(1).isEmpty) {
      throw new Exception("getVoFeedByExperimentSiteAndHostname returned an empty dataframe")
    }
    val dataset = df.persist()
    log.log(f"VoFeed metadata retrieved by experiment site and hostname for vo: $vo with prefix: $prefix")
    dataset
  }

  /**
    * Gets persisted VoFeed metadata from Kafka, grouped by experiment site.
    * The VO name has to be known for this query
    *
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @param vo     gets only the sites from this vo
    * @return VoFeed dataset with 3 columns: [prefix]+(experiment_site, site, tier)
    */
  def getVoFeedByExperimentSite(prefix: String, vo: Option[String] = None): Dataset[Row] = {
    val prefixedColumns: List[String] = List(
      "experiment_site", "site", "tier"
    )

    val partitionCols: List[String] = List(
      "data.experiment_site", "data.vo"
    )

    val df = metaDataSets.getFlattenTopology("vofeed_raw_topology", partitionCols, vo)
      .withColumnRenamed("official_site", "site")
      .select(col("vo") :: prefixedColumns.map(s => col(s).as(prefix + s)): _*)
    if (df.head(1).isEmpty) {
      throw new Exception("getVoFeedByExperimentSite returned an empty dataframe")
    }
    val dataset = df.persist()
    log.log(f"VoFeed metadata retrieved by experiment site for vo: $vo with prefix: $prefix")
    dataset
  }

  /**
    * Gets persisted VoFeed metadata from Kafka, grouped by hostname
    *
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @return VoFeed dataset with 4 columns: [prefix]+(hostname, experiment_site, site, tier)
    */
  def getVoFeedByHostname(prefix: String, vo: Option[String] = None): Dataset[Row] = {
    val prefixedColumns: List[String] = List(
      "hostname", "experiment_site", "site", "tier"
    )

    val partitionCols: List[String] = List(
      "data.hostname", "data.vo"
    )

    val df = metaDataSets.getFlattenTopology("vofeed_raw_topology", partitionCols, vo)
      .withColumnRenamed("official_site", "site")
      .select(col("vo") :: prefixedColumns.map(s => col(s).as(prefix + s)): _*)
    if (df.head(1).isEmpty) {
      throw new Exception("getVoFeedByHostname returned an empty dataframe")
    }
    val dataset = df.persist()
    log.log(f"VoFeed metadata retrieved by hostname for vo: $vo with prefix: $prefix")
    dataset
  }

  /**
    * Gets persisted Topology Metadata from topics:
    * - agis_raw_topology
    * - agis_raw_ddmendpoint
    * - rebus_raw_federations
    *
    * @return Map[String, Dataset] with topology metadata
    */
  def getTopologyMetadataBySourceTopic(): Map[String, Dataset[Row]] = {
    val map = Map(
      "agis_raw_topology" -> metaDataSets.agisTopology.persist(),
      "agis_raw_ddmendpoint" -> metaDataSets.agisDDMEndpoint.persist(),
      "rebus_raw_federations" -> metaDataSets.rebusFederations.persist(),
      "agis_raw_pandaqueue" -> metaDataSets.agisPandaqueue.persist()
    )
    log.log(f"Topology metadata retrieved by source topic")
    map
  }

  /**
    * Gets persisted topology metadata from Kafka.
    * This dataset is the set union of Rebus and Agis data with unique official sites.
    *
    * @param prefix the prefix for the column names (usually "src_" or "dst_")
    * @return Rebus+Agis dataset with 3 columns: [prefix]+(country, federation, site)
    */
  def getTopologyInfo(prefix: String): Dataset[Row] = {
    val dataset = metaDataSets.fullTopology
      .select(
        $"country".as(prefix + "country"),
        $"federation".as(prefix + "federation"),
        $"official_site".as(prefix + "site"))
      .persist()

    log.log(f"Rebus+Agis topology information retrieved with prefix: $prefix")
    dataset
  }
}
