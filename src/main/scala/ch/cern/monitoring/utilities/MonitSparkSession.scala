package ch.cern.monitoring.utilities

import org.apache.hadoop.security.UserGroupInformation
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object MonitSparkSession {

  def getSparkSession(applicationName: String, isLocal: Boolean, logger: Logger, logLevel: String): SparkSession = {
    var sparkConf = new SparkConf()

    if (isLocal) {
      sparkConf = sparkConf.setMaster("local[*]")
      UserGroupInformation.loginUserFromKeytab("monitops@CERN.CH", "/etc/monit/monitops.keytab")
    }

    val spark = SparkSession
      .builder
      .appName(applicationName)
      .config(sparkConf)
      .getOrCreate()

    spark.sparkContext.setLogLevel(logLevel)

    Logger.getRootLogger.setLevel(Level.toLevel(logLevel))
    logger.setLevel(Level.toLevel(logLevel, Level.INFO))

    spark
  }
}
