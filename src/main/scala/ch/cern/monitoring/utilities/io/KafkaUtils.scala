package ch.cern.monitoring.utilities.io

import org.apache.log4j.Logger
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import ch.cern.monitoring.utilities.logging.LoggerWrapper

/**
* Provides an interface to Kafka for input-output operations.
*/

case class KafkaUtils(spark: SparkSession, inputBrokers: String, outputBrokers: String, logger: Logger, generalOptions: Map[String,Any] = Map(), kafkaTopicPrefix: String = "") {

  def this(spark: SparkSession, inputBrokers: String, outputBrokers: String, logger: Logger, kafkaTopicPrefix: String) {

    this(spark, inputBrokers, outputBrokers, logger, Map(
      "kafka.enable.idempotence" -> false.toString,
      "kafka.security.protocol" -> "SASL_SSL",
      "kafka.sasl.mechanism" -> "GSSAPI",
      "kafka.sasl.kerberos.service.name" -> "kafka"),
      kafkaTopicPrefix
    )
  }

  val log = LoggerWrapper(logger, this.getClass.toString)

  def read(topic: String, options: Map[String,Any] = Map()): DataFrame = {
    var df = spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", inputBrokers)
      .option("subscribe", prependPrefix(topic))

    generalOptions ++ options foreach {
      case (key, value) => df = df.option(key, value.toString)
    }

    val dataframe = df.load
    log.log(f"Topic ${prependPrefix(topic)} read successfully")
    dataframe
  }

  def readWithSchema(topic: String, schemaPath: String, aliasName: String, options: Map[String,Any] = Map()): DataFrame = {
    var df = spark
      .read
      .format("kafka")
      .option("kafka.bootstrap.servers", inputBrokers)
      .option("subscribe", prependPrefix(topic))

    generalOptions ++ options foreach {
      case (key, value) => df = df.option(key, value.toString)
    }

    val dataFrame = df
      .load
      .select(from_json(col("value").cast("string"), Schemas.getSchema(spark, schemaPath)).alias(aliasName))
    log.log(f"Topic ${prependPrefix(topic)} read with schema $schemaPath successfully")
    dataFrame
  }

  def readStream(topic: String, options: Map[String,Any] = Map()): DataFrame = {
    var df = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", inputBrokers)
      .option("subscribe", prependPrefix(topic))

    generalOptions ++ options foreach {
      case (key, value) => df = df.option(key, value.toString)
    }

    val dataframe = df.load
    log.log(f"Topic ${prependPrefix(topic)} read as a stream successfully")
    dataframe
  }

  def readStreamWithSchema(topic: String, schemaPath: String, aliasName: String, options: Map[String,Any] = Map()): DataFrame = {
    var df = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", inputBrokers)
      .option("subscribe", prependPrefix(topic))
    generalOptions ++ options foreach {
      case (key, value) => df = df.option(key, value.toString)
    }

    val dataframe = df
      .load
      .select(from_json(col("value").cast("string"), Schemas.getSchema(spark, schemaPath)).alias(aliasName))
    log.log(f"Topic ${prependPrefix(topic)} read as a stream with schema $schemaPath successfully")
    dataframe
  }

  // A DataFrame is a Dataset[Row]
  def write[T](ds: Dataset[T], topic: String, options: Map[String, Any] = Map()): Unit = {
    var writeDs = ds
      .write
      .format("kafka")
      .option("kafka.bootstrap.servers", outputBrokers)
      .option("topic", prependPrefix(topic))
    generalOptions ++ options foreach {
      case (key, value) => writeDs = writeDs.option(key, value.toString)
    }

    writeDs.save
    log.log(f"Dataset $ds written to ${prependPrefix(topic)} successfully")
  }

  // A DataFrame is a Dataset[Row]
  def writeStream[T](ds: Dataset[T], topic: String, options: Map[String, Any] = Map(), outputMode: String = "append"): StreamingQuery = {
    var writeDs = ds
      .writeStream
      .format("kafka")
      .option("kafka.bootstrap.servers", outputBrokers)
      .option("topic", prependPrefix(topic))
      .outputMode(outputMode)
    generalOptions ++ options foreach {
      case (key, value) => writeDs = writeDs.option(key, value.toString)
    }

    val streamingQuery = writeDs.start()
    log.log(f"Dataset $ds started to be written into ${prependPrefix(topic)} successfully")
    streamingQuery
  }

  /**
   * Prepends the global prefix name to the given topic such that prefix+topic are joined without any character
   * in between.
   * @param topic to be prepended with the global prefix.
   * @return a new String object formatted as $this.kafkaTopicPrefix$topic.
   */
  private[this] def prependPrefix(topic: String): String = {
    f"$kafkaTopicPrefix$topic"
  }
}