package ch.cern.monitoring.utilities.io

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.log4j.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.time.{Duration, Instant, ZoneOffset}
import scala.collection.mutable.ListBuffer

object HdfsUtils {
  def getHDFSPathToReadFrom(hdfsRoot: String, startTime: Instant, endTime: Instant): List[String] = {
    val hdfsRootPath = new Path(hdfsRoot)
    val startDateTime = startTime.truncatedTo(ChronoUnit.DAYS)
    val endDateTime = endTime.truncatedTo(ChronoUnit.DAYS)
    val timeDiffInDays = Duration.between(startDateTime, endDateTime).toDays.toInt

    val listOfResultHDFSPaths = new ListBuffer[String]()
    for (i <- 0 to timeDiffInDays) {
      listOfResultHDFSPaths += hdfsRootPath
        .suffix("/".concat(DateTimeFormatter.ofPattern("yyyy/MM/dd")
          .format(startDateTime.plus(i, ChronoUnit.DAYS).atZone(ZoneOffset.UTC)))).toString
    }
    listOfResultHDFSPaths.toList
  }

  def getHDFSPathToReadFromWithTopic(hdfsRoot: String, topic: String, startTime: Instant, endTime: Instant): List[String] = {
    val hdfsRootPath = new Path(hdfsRoot)
    val startDateTime = startTime.truncatedTo(ChronoUnit.DAYS)
    val endDateTime = endTime.truncatedTo(ChronoUnit.DAYS)
    val timeDiffInDays = Duration.between(startDateTime, endDateTime).toDays.toInt

    val listOfResultHDFSPaths = new ListBuffer[String]()
    for ( i <- 0 to timeDiffInDays ) {
      listOfResultHDFSPaths += hdfsRootPath
        .suffix("/".concat(topic.replaceAll("_", "/")))
        .suffix("/".concat(DateTimeFormatter.ofPattern("yyyy/MM/dd")
          .format(startDateTime.plus(i, ChronoUnit.DAYS).atZone(ZoneOffset.UTC)))).toString
    }
    listOfResultHDFSPaths.toList
  }
  
}

case class HdfsUtils (spark: SparkSession, logger: Logger) {
  def getAllExistingHDFSFolders(hdfsRoot: String): List[String] = {
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    val hdfsRootPath = new Path(hdfsRoot)

    val listOfHDFSPaths = new ListBuffer[String]()
    val listOfYearFolders = new ListBuffer[Path]()
    val listOfMonthFolders = new ListBuffer[Path]()
    val listOfDayFolders = new ListBuffer[Path]()

    // get all the 'year' folders
    listOfYearFolders ++= fs.listStatus(hdfsRootPath).map(f => f.getPath)
    for (yearF <- listOfYearFolders) {
      listOfMonthFolders ++= fs.listStatus(yearF).map(f => f.getPath)
    }
    for (monthF <- listOfMonthFolders) {
      listOfDayFolders ++= fs.listStatus(monthF).map(f => f.getPath)
    }

    listOfHDFSPaths ++= listOfDayFolders.map(f => f.toString)

    listOfHDFSPaths.toList
  }

  def getHDFSFoldersIfExisting(hdfsRoot: String, startTime: Instant, endTime: Instant): List[String] = {
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    val result = HdfsUtils.getHDFSPathToReadFrom(hdfsRoot, startTime, endTime)

    result.filter(f => fs.exists(new Path(f)))
  }

  def getHDFSFoldersWithTopicIfExisting(hdfsRoot: String, topic: String, startTime: Instant, endTime: Instant): List[String] = {
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    val result = HdfsUtils.getHDFSPathToReadFromWithTopic(hdfsRoot, topic, startTime, endTime)

    result.filter(f => fs.exists(new Path(f)))
  }

  def read(path: String, start: Instant, end: Instant): DataFrame = {
    spark
      .read
      .json(getHDFSFoldersIfExisting(path, start, end): _*)
  }

  def readWithTopic(path: String, topic: String, start: Instant, end: Instant): DataFrame = {
    spark
      .read
      .json(getHDFSFoldersWithTopicIfExisting(path, topic, start, end): _*)
  }

  def readWithTopicAndSchema(path: String, topic: String, schemaPath: String, start: Instant, end: Instant): DataFrame = {
    spark
      .read
      .schema(Schemas.getSchema(spark, schemaPath))
      .json(getHDFSFoldersWithTopicIfExisting(path, topic, start, end): _*)
  }

  def readAll(path: String): DataFrame = {
    spark
      .read
      .json(getAllExistingHDFSFolders(path): _*)
  }
}
