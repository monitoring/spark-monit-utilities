package ch.cern.monitoring.utilities.io

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType

object Schemas {
  def getSchema(spark: SparkSession, schemaPath: String): StructType = {
    spark
      .read
      .json(schemaPath)
      .schema
  }
}
